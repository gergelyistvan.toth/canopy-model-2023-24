#name: Yeji
#date: from Wed Dec  18 
#author (in order of contributing): Yeji

# READ ME (aim of the script, equations covered, ...) ##########################
'



'
par(mfrow=c(1,1))
Sys.setenv(LANG = "en")

# LOAD AND INSTALL LIBRARIES ###################################################

#library()
#library()

# READING INPUT DATA ###########################################################

## DATA WE ALL NEED #####

data_flux <- read.csv(here::here("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))
data_meteo <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))
data_soil <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv"))


## ADDITIONAL DATA #####

#data <- read.table(here::here("4_water_cycle/..."), header=T)         #for txt in the subgroup
#data <- read.table(here::here("..."), header=T)                       #for txt in the overall project

#data <- read.csv(here::here("measurements/..."))                      #for csv in the measurements
#data <- read.csv(here::here("4_water_cycle/..."))                     #for csv in the subgroup





# SETTING PARAMETERS ###########################################################

Parameters <- read.csv(here::here("measurements/parameters_new.csv"), sep = ",")
Parameters
Parameters$variable # view parameters


# create variables with the parameter values







# DEFINING FUNCTIONS ###########################################################

library(base)
library(lubridate)
library(dplyr)
library(purrr)

source(here::here("utility/setup_parameters.R"))


#' get_day_LAI(now()) # Get the LAI of the current moment

#' days <- as_datetime(c("2021-01-01", "2021-05-01", "2021-07-01", "2021-10-15", "2021-12-01"))
#' get_day_LAI(days)
get_day_LAI <- function(time) {
    
    stopifnot(all(is.POSIXct(time)))
    ##  --- This is some complex code to be able to support timesteps smaller than a day
    # convert the days of years in dates and the integer
    start_year <- floor_date(time, "year") #first day of the year
    pars_date <- pars %>% 
        select(leaf_out, leaf_full, leaf_fall, leaf_fall_complete) %>% 
        # convert days to seconds
        map_df(~as_datetime(.x * 86400, origin=start_year) %>% as.integer)
    time <- as.integer(time) # convert to integer for easy comparison
    ## ---
    
    case_when(
        # Winter LAI is 0
        time < pars_date$leaf_out ~ 0,
        # spring LAI has linear increase
        time >= pars_date$leaf_out & time < pars_date$leaf_full ~ {
            ndays <- pars_date$leaf_full - pars_date$leaf_out # n days of the transition
            pars$max_LAI * (time - pars_date$leaf_out) / ndays
        },
        # summer LAI is max
        time >= pars_date$leaf_full & time < pars_date$leaf_fall ~ pars$max_LAI,
        # Autumn LAI is decreasing
        time >= pars_date$leaf_fall & time < pars_date$leaf_fall_complete ~ {
            ndays <- pars_date$leaf_fall_complete - pars_date$leaf_fall # n days of the transition
            pars$max_LAI * (pars_date$leaf_fall_complete - time) / ndays
        },
        # winter again
        time >= pars$leaf_fall_complete ~ 0
    )
}

data_meteo$TIMESTAMP_END <- as.POSIXct(data_meteo$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S")

plot(get_day_LAI(data_meteo$TIMESTAMP_END))
class(time)



# ACTUAL SCRIPT ################################################################
interception <- data_meteo[ , c("TIMESTAMP_END", "PREC_mm")]
data_meteo$TIMESTAMP_END <- as.POSIXct(data_meteo$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S")
interception$LAI <- get_day_LAI(data_meteo$TIMESTAMP_END)

interception$WcMax <- 0.2*interception$PREC_mm*interception$LAI 

interception$WcMax <- ifelse(interception$WcMax > interception$PREC_mm, interception$PREC_mm, interception$WcMax)

interception$WcMax[2066] = 0
interception$WcMax[10802] = 0

plot(interception$WcMax)

# WcAct = Actual water that leaves can hold
# Using a for loop to iterate through each row

# Leaves can hold the water between 0 to WcMax. The actual water holding is WcAct. If WcAct holds more than WcMax, then it leaves only WcMax and the rest fall through. 

# Assumption: We will assume tree water layer can hold maximum 2mm (we can change this value)
max_TW_hold <- interception$WcMax

# Assign temporarily value to the drip
interception$drip <- 0


for (i in 1:nrow(interception)) {
    # Check for missing values and non-numeric values in WcAct and WcMax
    if (!is.na(interception$WcAct[i]) && !is.na(interception$WcMax[i]) &&
        is.numeric(interception$WcAct[i]) && is.numeric(interception$WcMax[i])) {
        
        #cumulative_precipitation <- cumsum(interception$PREC_mm[1:i])
        
        if (any(interception$WcAct[i] <= max_TW_hold)) {
            interception$WcAct[i] <- interception$WcAct[i-1]+interception$PREC_mm[i]
            interception$drip[i] <- 0
        } else {
            interception$WcAct[i] <- interception$WcAct[i-1]
            interception$drip[i] <- interception$PREC_mm[i]
        }
    }
}

# WRITING OUTPUT ###############################################################






# PLOTTING FIGURES #############################################################






# CLEAN UP #####################################################################

rm()


