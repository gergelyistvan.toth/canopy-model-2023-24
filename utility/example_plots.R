### Exemplary plot scripts
### to compare model simulations with measurements
### just to get started

library(tidyverse)
library(here)
library(ggplot2)
library(lubridate)
library(zoo)

#### Reading exemplary data ####
meteo <- c("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)

#meteo <- read_csv(here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))

meteo <- meteo %>% 
  select(TIMESTAMP_END, meas=TA_degC) %>% 
  mutate(mod = meas*runif(1) + runif(n())) # generate fake model data with random mean + random noise 

y_min <- min(meteo$meas, meteo$mod)
y_max <- max(meteo$meas, meteo$mod)




#### Plot: Timeseries ####
plt_timeseries <- ggplot(data=meteo, aes(x=TIMESTAMP_END)) +
  geom_line(aes(y=mod, color="simulations")) +
  geom_line(aes(y=meas, color="observations")) +
  geom_hline(yintercept=0, linetype="dashed", color="black") +
  scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
  xlab('time') +
  ylab('variable name (unit)') +
  scale_color_manual(name="", values=c("darkblue", "darkred")) +
  #theme_classic()
  theme_light()
plt_timeseries
ggsave("plot_timeseries.png", width=10, height=6, dpi=300)



#### Plot: Scatter ####
ggplot(data=meteo) +
  geom_point(aes(x=meas, y=mod), size=1, color='grey30', alpha=.5) +
  geom_smooth(aes(x=meas, y=mod), method="lm", fullrange=TRUE, color="red") +      # adding regression line
  geom_hline(yintercept=0, linetype="dashed", color="black") +
  geom_vline(xintercept=0, linetype="dashed", color="black") +
  geom_abline(slope=1, linetype="dashed", color="black") +
  xlab('observed variable (unit)') +
  ylab('simulated variable (unit)') +
  xlim(y_min - 0.1*y_min, y_max + 0.1*y_max) +
  ylim(y_min - 0.1*y_min, y_max + 0.1*y_max) +
  theme_light() +
  theme(aspect.ratio=1, legend.position="none")
ggsave("plot_scatter.png", width=6, height=5, dpi=300)



#### Plot: Mean diurnal variation ####

# aggregate data
diurnal <- meteo %>% 
  group_by(hour=hour(TIMESTAMP_END)) %>% 
  summarize(meas_mean=mean(meas), meas_sd=sd(meas), mod_mean=mean(mod), mod_sd=sd(mod))


ggplot(data=diurnal, aes(x=hour)) +
  # Model
  geom_line(aes(y=mod_mean, color="simulations"), size=2) +
  geom_errorbar(aes(ymin=(mod_mean - mod_sd), ymax=(mod_mean + mod_sd), color="simulations")) +
  # Measurements
  geom_line(aes(y=meas_mean, color="observations"), size=2) +
  geom_errorbar(aes(ymin=(meas_mean - meas_sd), ymax=(meas_mean+meas_sd), color="observations")) +
  geom_hline(yintercept=0, linetype="dashed", color="black") +
  xlab('time of day') +
  ylab('mean variable name (unit)') +
  scale_color_manual(name="", values = c("darkred", "darkblue")) +
  theme_light()
ggsave("plot_diurnal.png", width=6, height=5, dpi=300)


## or checkout library(esquisse)