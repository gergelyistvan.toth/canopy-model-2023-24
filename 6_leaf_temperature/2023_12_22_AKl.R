library(here)

## 1. Open Data sets
# 1.1. Name and View 

##AKl##parameters <- read.csv2(here::here ("measurements/parameters_new.csv"))

##AKl##meteo <- read.csv2(here::here ("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))

##AKl##fluxes <- read.csv2(here::here ("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))

parameters <- read.csv2(here::here ("measurements/parameters_new.csv"), sep=',')   ##AKl##

meteo <- read.csv2(here::here ("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"), sep=',')   ##AKl##

fluxes <- read.csv2(here::here ("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"), sep=',')   ##AKl##

# 1.2. Merge the data sets "meteo" and "fluxes" by timestamps

df <- merge(meteo, fluxes, by = "TIMESTAMP_END")

# 1.3. Delete unnecessary columns from dataframe

library(dplyr)
df <- df %>% select(-PPFD_IN_umolm.2s.1, -PPFD_OUT_umolm.2s.1, -PREC_mm, -WD_deg, -CO2conc_ppm, 
                    -NEE_umolm.2s.1, -GPP_DT_umolm.2s.1, -RECO_DT_umolm.2s.1, -NEE_DT_umolm.2s.1)
View(df)

############################## Leaf temperature (K) ###########################
##AKl##calc_Tl <- as.numeric(df$TA_K)
calc_Tl <- as.numeric(df$TA_degC) + 273.15   ##AKl##
df[,"Tl_K"] <- calc_Tl
View(df)

############ Calculate leaf boundary layer conductance ###############

##AKl##Ta <- as.numeric(df$TA_K) # Air temperature (K)
##AKl##P <- as.numeric(df$PRESS_hPa) # Air pressure (Pa)
Ta <- as.numeric(df$TA_degC) + 273.15   # Air temperature (K)   ##AKl##
P <- as.numeric(df$PRESS_hPa)*100   # Air pressure (Pa)     ##AKl##
WS <- as.numeric (df$WS_ms.1)   # Wind speed (m/s)
Tl <- calc_Tl   # Leaf temperature (K)
d <- 0.05 # Leaf dimension (m)

calc_gb <- function(Ta, P, WS, Tl, d=0.05){
  
  # Physical constants
  Tfrz <- 273.15      # Freezing point of water (K)
  g <- 9.80616        # Gravitational acceleration (m/s2)
  Rgas <- 8.31447     # Universal gas constant (J/K/mol)
  visc0 <- 13.3e-06   # Kinematic viscosity at 0 degC and 1013.25 hPa (m2/s)
  Dh0 <- 18.9e-06     # Molecular diffusivity (heat) at 0 degC and 1013.25 hPa (m2/s)
  Dv0 <- 21.8e-06     # Molecular diffusivity (H2O) at 0 degC and 1013.25 hPa (m2/s)
  
  # Input variables
  rhomol <- numeric() # Air density
  rhomol <- P / (Rgas * Ta)   # Molar density (mol/m3)
  
  Re <- numeric() # Re = Renolds number
  
  gbh <- numeric()   # leaf boundary layer conductance for heat (mol/m2/s)
  gbw <- numeric()   # leaf boundary layer conductance for water vapor (mol/m2/s)
  
  # Adjust diffusivity for temperature and pressure
  # following Tab. A.3, p. 381
  # fac = Calculated diffusion coefficient for a given temperature and pressure
  fac <- 101325 / P * (Ta / Tfrz)^1.81
  
  visc <- visc0 * fac   # Kinematic viscosity (m2/s)
  Dh <- Dh0 * fac       # Molecular diffusivity, heat (m2/s)
  Dv <- Dv0 * fac       # Molecular diffusivity, H2O (m2/s)
  
  # Dimensionless numbers
  Re <- WS * d / visc                              # Reynolds number, eq. 10.32
  Pr  <- visc / Dh                                       # Prandtl number, eq. 10.31
  Scv <- visc / Dv                                       # Schmidt number for H2O, eq. 10.37
  Gr <- g * d^3 * (Tl - Ta) / (Ta * visc^2)   # Grashof number, eq. 10.42
  #  print(c(Re, Gr))
  
  
  b1 <- 1.5                              # Empirical correction factor for Nu and Sh
  
  if (Gr/(Re^2) < 1){   # ratio of buoyancy to inertial forces
    
    if (Re < 2100){
      # Forced convection - laminar flow
      Nu  <- b1 * 0.66 *  Pr^0.33 * Re^0.5   # Nusselt number, eq. 10.33
      Shv <- b1 * 0.66 * Scv^0.33 * Re^0.5   # Sherwood number, H2O, eq. 10.38
      flag <- 1                              # indicator for forced convection, laminar flow
    }else{
      # Forced convection - turbulent flow
      Nu  <- b1 * 0.036 *  Pr^0.33 * Re^0.8   # Nusselt number, eq. 10.40
      Shv <- b1 * 0.036 * Scv^0.33 * Re^0.8   # Sherwood number, H2O, eq. 10.41
      flag <- 2                               # indicator for forced convection, turbulent flow
    }
    
  }else{
    # Free convection
    if (Gr < 10^5){
      Nu  <- 0.54 *  Pr^0.25 * Gr^0.25   # Nusselt number, eq. 10.43
      Shv <- 0.54 * Scv^0.25 * Gr^0.25   # Sherwood number, H2O, eq. 10.44
    }else{
      Nu  <- 0.15 *  Pr^0.33 * Gr^0.33   # Nusselt number, eq. 10.45
      Shv <- 0.15 * Scv^0.33 * Gr^0.33   # Sherwood number, H2O, eq. 10.46
    }
    flag <- 3                              # indicator for free convection, laminar flow
  }
  
  
  gbh <- Dh * Nu / d * rhomol    # Boundary layer conductance, heat (mol/m2/s), eq. 10.30
  gbw <- Dv * Shv / d * rhomol   # Boundary layer conductance, H2O (mol/m2/s), eq. 10.39 & 10.30
  
  
  results <- data.frame("gbh"=gbh, "gbw"=gbw, "flag"=flag)
  return(results)
}

######################### Loop gbh and gbw per hour ############################

gbw<- NA 
gbh<- NA # You need this that's it no explanation

for (h in 1:nrow(df)){
  results <- calc_gb(Ta[h], P[h], WS[h], Tl[h], d=0.05) # Loop for gbh per hour
  # d=0.05 is a constant, therefore not by hour
  
  gbh[h] <- results$gbh # calling the function for gbh for each hour<<
  gbw[h] <- results$gbw # calling the function for gbw for each hour
  
}
View (gbh)
View (gbw)

df[,"gbh"] <- gbh
df[,"gbw"] <- gbw
View(df)

##AKl## adjust general structure of functions: pull out definitions of variables
############ Sensible heat flux of leaf (W*m−2): Fick's Law (Bonan eq.10.5)##########
 calc_H <- function (Tl, Ta, gbh){

   # Input variables
   Cp <- 29.3    # Specific heat capacity of air (J*mol-1*K-1)
   Tl <- as.numeric(df$Tl_K) # Leaf surface temperature (K)
   Ta <- as.numeric(df$TA_K) # Air temperature (K)
   gbh <- as.numeric(df$gbh) # Boundary layer conductance for heat (mol*m-2*s-1)
   
   # Calculate the Sensible heat flux
   H <- 2*Cp*(Tl-Ta)*gbh
   
   ##AKl##results <- ("H"= H)
   ##AKl##return(H)
   results <- data.frame("H"=H)
   return(results)
 }

############################### Loop H per hour ################################

H <- NA
for (h in n:row(df)) {
  results <- calc_H(Tl[h], Ta[h], gbh [h])
  H[h] <- results$H
}

####################### Latent heat flux of leaf (W*m−2) #######################
 calc_L <-function (gbw, VPD){
   
   # Input variables
    VPD_hPa <- as.numeric(df$VPD_hPa) # Vapor pressure deficit (es(Tl)-ea)
    VPD_kPa <- 0.1*VPD_hPa # Vapor pressure deficit (kPa)
    ##AKl##gbw <- calc_gbw # Boundary layer conductance for water (mol*m-2*s-1)
    gbw <- df$gbw # Boundary layer conductance for water (mol*m-2*s-1)  ##AKl##
    Cp <- 29.3  # Specific heat capacity of air  (J*mol-1*K-1)
    PC <- 0.067 # Psychometric constant "gamma" (kPa*K-1)
    
    
    #Calculate the Latent heat flux 
    #with formula: L = ((gbw*Cp)/ Gamma)*(es(Tl)- ea) 
    
  L <- ((gbw*Cp)/PC)*VPD_kPa
  return(L)
 }
View(L)
###################### radiative forcing #########################



###################### Net radiation (Bonan eq. 10.22) #########################
Calc_Rn <- function(sw_in, sw_out, lw_in, T_l){
# Rn = Qa - 2*LE*SBC*(T_l^4)
# Qa = sw_in - sw_out + lw_in
  #Input variable
  sw_in <- df$SW_IN_Wm.2   # Incoming short wave radiation (W*m-2)
  sw_out <- df$SW_OUT_Wm.2   # Outgoing short wave radiation (W*m-2)
  lw_in <- df$LW_IN_Wm.2   # Incoming long wave radiation (W*m-2)
  LE <- 0.98   # Leaf emissivity (Unitless)
  SBC <- 5.67*10**-8   # Stefan bolzmann constant (W*m-2*K-4)
  
  # Calculate the Net radiation
  Rn <- sw_in - sw_out + lw_in - 2 * LE * SBC * (T_l**4)
  return(Rn)
}  

########################### Leaf temperature (K) ###############################

T_l <- Rn - H - L   ##AKl### devide by cL and this is the temporal change
T_l <- 1- H - L
