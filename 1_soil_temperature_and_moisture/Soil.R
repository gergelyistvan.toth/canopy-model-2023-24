#### open file ####
##install packages 
install.packages("here")
install.packages("ggplot2")

##load libraries 
library(here)
library(dplyr)
library(ggplot2)
library(lubridate)
library(ggplot2)
source(here::here("utility/canopy_parameters.R"))

setwd(here::here())

##load data sets 
soildata_16_17 <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv")) #Measurements_soil_hourly_201601_201712_gapfilled
meteodata_16_17 <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv")) #Measurements_meteo_hourly_201601_201712_gapfilled
parameters_1 <- read.csv((here::here("measurements/parameters.csv")))
parameters_2 <- read.csv((here::here("measurements/parameters_new.csv")))

soildata_16_17$TIMESTAMP_END <-as.POSIXct(soildata_16_17$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S", tz='Etc/GMT-1') #Correct any errors in timestamp

##Adding necessary data to soildata_16_17 from other data sets
soildata_16_17$PREC_mm <- meteodata_16_17$PREC_mm #for soil moisture calculation
soildata_16_17$Air_temp <- meteodata_16_17$TA_degC #for soil evaporation calculation
####

####################################################
#### 1. Bucket model based on 8 cm (Silty clay) ####
####################################################

temp_gc <- 0.8 # mol/m^2*s # conductivity
# soil depth = 8 cm = 80 mm
# sand 2-3 %, silt 46 %, clay 52-51 % -> silty clay
# Soil moisture difference = R - E - I (+R)

## Temporary variables 
# evaporation rate = 0.0 : "E" in bucket model equation, need to be measured
# soil hydraulic conductivity (8 cm, Silty clay) = 0.05 cm/h = 0.5 mm/h
# infiltration rate (8 cm, silty clay) = 1 mm/h
# Runoff = 0
# Effective porosity (8 cm, silty clay) = 0.423 cm^3 / cm^3

# Assume : soil area = 1 cm^2 = 100 mm^2
# Soil volume = 80 * 100 = 8000 mm^3
# Soil effective pore[mm] = 80 * 0.423 = 33.84 mm
cp <- 2.93000e+01 # J/mol*K # specific heat of moist air 
lambda <-  2.45 #MJ/kg
soil_area <- 100 # mm^2
soil_hydraulic_conductivity <- 0.5 # mm/h
infiltration_rate_8 <- 1 # mm/h
SWC_limit_8 <- 33.84 # mm

## Net Radiation
net_rad <- rep(NA, nrow(soildata_16_17))
net_rad[1] <- 0

for (i in 1:nrow(soildata_16_17)) {
    net_radiation <- meteodata_16_17$LW_IN_Wm.2[i] - meteodata_16_17$LW_OUT_Wm.2 + 
        meteodata_16_17$SW_IN_Wm.2 - meteodata_16_17$SW_OUT_Wm.2
    net_rad[i] <- net_radiation
}
soildata_16_17$net_rad <- net_rad # W/m^2

## Soil radiation
soil_rad <- rep(NA, nrow(soildata_16_17))
soil_rad[1] <- 0

soildata_16_17$Daily_LAI <- get_day_LAI(soildata_16_17$TIMESTAMP_END)
for(i in 1:nrow(soildata_16_17)){
    soil_radiation <- soildata_16_17$net_rad[i] * exp(-0.6 * soildata_16_17$Daily_LAI[i])
    soil_rad[i] <- soil_radiation
}
soildata_16_17$soil_rad <- soil_rad # W/m^2

#### Soil evaporation ####
df <- data.frame(time = soildata_16_17$TIMESTAMP_END)

# Gamma [kPa/degC]
gamma <- 0.665 * 10^-3 * 101.325

# Thr, hourly air temperature [degC]
Thr <- meteodata_16_17$TA_degC
Thr[Thr == 0] <- 0.001 # 0 degC doesn't work with exp
df$Thr <- Thr

# Delta [kPa/degC]
delta <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    delta[i] <- (4098 * (0.6108 * exp((17.27 * df$Thr[i]) 
                                      / (df$Thr[i] + 237.3))) / (df$Thr[i])^2)
}
df$delta <- delta

# Rn, net radiation [MJ/m2*hr]
Rn <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    Rn[i] <- soildata_16_17$soil_rad[i] / 10^6 / 60 / 24 # W/m2 to MJ/m2*hr
}
df$Rn <- Rn

# G, soil heat flux density [MJ/m2*hr]
G <- soildata_16_17$G_5cm_Wm.2 / 10^6 / 60 / 24 # W/m2 to MJ/m2*hr
df$G <- G

# u2, wind velocity at 2 m [m/s]
u2 <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    u2[i] <- meteodata_16_17$WS_ms.1[i] * 4.87 / log(67.8 * 44 - 5.42)
}
df$u2 <- u2

# VPD, vapor pressure deficit [kPa]
VPD <- meteodata_16_17$VPD_hPa / 10 # hPa to kPa
df$VPD <- VPD

# Hourly evaporation
ex_evap <- rep(NA, nrow(soildata_16_17))
for (i in 1:nrow(soildata_16_17)) {
    ex_evap[i] <- (0.408 * delta[i] * (Rn[i] - G[i]) + gamma * 37 / (Thr[i] + 273.15) * u2[i] * VPD[i]) /
        (delta[i] + gamma * (1 + 0.34 * u2[i]))
}
df$evap <- ex_evap

ggplot(df, aes(x = time, y = evap)) +
    geom_line() +
    labs(title = "Soil Evaporation over Time")

soildata_16_17$soil_evap <- ex_evap


## Soil moisture bucket model
soil_moisture <- rep(NA, nrow(soildata_16_17))
soil_moisture[1] <- 39.648 / 100 * 80 # % to mm

for (i in 2:nrow(soildata_16_17)) {
    change_in_moisture <- soildata_16_17$PREC_mm[i] - soildata_16_17$soil_evap[i] 
    soil_moisture[i] <- soil_moisture[i-1] + change_in_moisture 
}
soildata_16_17$soil_moisture <- soil_moisture # mm/h

### Visualization 
ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = soil_moisture, color = "soil_moisture")) +
    geom_line(aes(y = SWC_8cm_. / 100 * 80, color = "Soil_Water"))





###########################
#### 2. Multiple layer ####
###########################

## Soil moisture
soil_moisture_8 <- rep(NA, nrow(soildata_16_17))
soil_moisture_8[1] <- 39.648 / 100 * 80
infiltraion_from_8 <- rep(NA, nrow(soildata_16_17))
infiltraion_from_8[1] <- 0

# For 8cm soil column 
for (i in 2:nrow(soildata_16_17)) {
    if (soil_moisture_8[i-1] >= SWC_limit_8) { # Saturation limit (max)
        soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$PREC_mm[i-1] -
            soildata_16_17$soil_evap[i-1] - infiltration_rate_8 
        infiltraion_from_8[i] <- infiltration_rate_8
    }
    else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.3)) { # EXAMPLE Field capacity limit (Min)
        soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$PREC_mm[i-1] 
        infiltraion_from_8[i] <- 0
    }
    else {
        soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$PREC_mm[i-1] -
            soildata_16_17$soil_evap[i-1] # Calculate change in soil moisture
        infiltraion_from_8[i] <- 0
    }
}
soildata_16_17$Soil_Moisture_8cm <- soil_moisture_8 #Update soildata_16_17 table
soildata_16_17$Infiltration_8cm <- infiltraion_from_8

ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Soil_Moisture_8cm, color = "Soil_Moisture_8cm")) + 
    geom_line(aes(y = SWC_8cm_. / 100 * 80, color = "SWC_8cm (actual)")) + # % to mm
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Soil Moisture Over Time at 8cm") +
    scale_color_manual(values = c("black", "red")) +
    theme_minimal()
# Maximum doesnt work??

# For 16cm soil column
# sand 3 %, silt 46-47 %, clay 51-50 % -> silty clay
soil_moisture_16 <- rep(NA, nrow(soildata_16_17))
soil_moisture_16[1] <- 38.290 / 100 * 80

for (i in 2:nrow(soildata_16_17)) {
    if (soil_moisture_16[i-1] >= SWC_limit_8) { 
        soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i-1] -
            soildata_16_17$soil_evap[i-1] - infiltration_rate_8 #Saturation limit (Max)
    }
    else if (soil_moisture_16[i-1] <= (SWC_limit_8 * 0.3)) {
        soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i-1]
    }
    else {
        soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i-1] -
            soildata_16_17$soil_evap[i-1]
    }
}
soildata_16_17$Soil_Moisture_16cm <- soil_moisture_16 #Update soildata_16_17 table

ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Soil_Moisture_16cm, color = "Soil_Moisture_16cm")) +
    geom_line(aes(y = SWC_16cm_. / 100 * 80, color = "SWC_16cm (actual)")) +  # % to mm
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Soil Moisture Over Time at 16cm") +
    scale_color_manual(values = c("black", "red")) +
    theme_minimal()

# calculated soil moisture at multiple layer
ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Soil_Moisture_16cm, color = "Soil_Moisture_16cm")) +
    geom_line(aes(y = Soil_Moisture_8cm, color = "Soil_Moisture_8cm")) +  # % to mm
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Soil Moisture Over Time") +
    scale_color_manual(values = c("red", "orange")) +
    theme_minimal()
